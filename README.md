#Bananify [![Build Status](https://magnum.travis-ci.com/mtlewis/es6-generator-mapreduce.svg?token=mRuJAszN1yL2cY9ebqCq&branch=master)](https://magnum.travis-ci.com/mtlewis/es6-generator-mapreduce)

Map-reduce library leveraging ES6 generators to optimize retrieval of sub lists.

##But I use LoDash / Underscore for that?

Yeah but each operation you perform on a collection with one of those libraries will iterate over *every* element in the collection.  Bananify waits until you actual want to enumerate the members of the collection before doing anything, so you can perform complex operations on huge collections of data and then just grab the elements as you need them.

##Example

```javascript
let bananify = require('bananify');

let myArray = [1,2,3,4,5,6,7,8,9,10];
let myBanana = bananify(myArray);
let firstThreeEvens = myBanana.filter(num => num % 2 === 0)
                              .take(3)
                              .enumerate(); // [2,4,6], filter function called exactly 6 times
```

Cool, right?
