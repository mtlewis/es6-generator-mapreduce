'use strict';

module.exports = function* map(generator, fn) {
    let result;

    while(result = generator.next(), !result.done) {
        yield fn(result.value);
    }
};
