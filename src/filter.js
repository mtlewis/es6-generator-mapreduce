'use strict';

module.exports = function* filter(generator, fn) {
    let result;

    while(result = generator.next(), !result.done) {
        if (fn(result.value)) {
            yield result.value;
        }
    }
};
