'use strict';

module.exports = function* take(generator, count) {
    let result;

    while(result = generator.next(), !result.done) {
        if (count > 0) {
            count--;
        } else {
            yield result.value;
        }
    }
};
