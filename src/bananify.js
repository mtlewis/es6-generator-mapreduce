'use strict';

var filter = require('./filter');
var map = require('./map');
var skip = require('./skip');
var take = require('./take');

module.exports = function _(collection) {
    var generator = collection.values();

    var banana = {
        values: function() { return generator; },
        all: function() {
            var result = [];

            for(var val of generator) {
                result.push(val);
            }

            return result;
        },
        filter: function(filterFn) {
            generator = filter(generator, filterFn);

            return banana;
        },
        map: function(mapFn) {
            generator = map(generator, mapFn);

            return banana;
        },
        skip: function(count) {
            generator = skip(generator, count);

            return banana;
        },
        take: function(count) {
            generator = take(generator, count);

            return banana;
        }
    };

    return banana;
};
