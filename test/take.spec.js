'use strict';

var expect = require('chai').expect;

describe('#take', function() {
    var bananify = require('../src/bananify');

    it("should retrieve the requested number of values from the passed-in generator", function() {
        var takeBanana = bananify([0,1,2,3,4,5,6,7,8,9]).take(5);
        expect(takeBanana.all()).to.deep.equal([0,1,2,3,4]);
    });
});
