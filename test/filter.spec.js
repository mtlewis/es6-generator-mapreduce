'use strict';

var expect = require('chai').expect;
var sinon = require('sinon');

describe('filter', function() {
    var bananify, banana, isEven;

    beforeEach(function() {
        bananify = require('../src/bananify');
        isEven = sinon.spy(function isEven(num) { return num % 2 === 0; });
        banana = bananify([0,1,2,3,4,5,6,7,8,9]).filter(isEven);
    })

    it("should generate", function() {
        expect(banana.all()).to.deep.equal([0,2,4,6,8]);
        expect(isEven.callCount).to.equal(10);
    });

    it("should work in conjunction with #take", function() {
        expect(banana.take(3).all()).to.eql([0,2,4]);
        expect(isEven.callCount).to.equal(5);
    });

    it("should work in conjunction with #skip", function() {
        expect(banana.skip(2).take(3).all()).to.eql([4,6,8]);
        expect(isEven.callCount).to.equal(9);
    });
});
