'use strict';

var expect = require('chai').expect;
var sinon = require('sinon');

describe('map', function() {
    var bananify, banana, timesTwo;

    beforeEach(function() {
        bananify = require('../src/bananify');
        timesTwo = sinon.spy(function timesTwo(num) { return num*2; });
        banana = bananify([0,1,2,3,4,5,6,7,8,9]).map(timesTwo);
    })

    it("should work with arrays", function() {
        expect(banana.all()).to.deep.equal([0,2,4,6,8,10,12,14,16,18]);
    });

    it("should work in conjunction with #take", function() {
        expect(banana.take(3).all()).to.deep.equal([0,2,4]);
        expect(timesTwo.callCount).to.equal(3);
    });

    it("should work in conjunction with #skip", function() {
        expect(banana.skip(2).take(3).all()).to.deep.equal([4,6,8]);
        expect(timesTwo.callCount).to.equal(5);
    });
});
